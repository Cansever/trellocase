import os
import time
from celery import Celery

from db.repository.task import check_status_tasks


celery = Celery(__name__)
celery.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://redis:6379")
celery.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://redis:6379")


@celery.task(name="celery_task")
def celery_task(type,current_user_id):
    if type == "check_task_status":
        result = check_status_tasks(current_user_id=current_user_id)
        return result
    return True