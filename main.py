from api.base import api_router
from core.config import settings
from db.base import Base
from db.session import engine
from db.utils import check_db_connected
from db.utils import check_db_disconnected
from fastapi import FastAPI 
from apps.base import api_router as web_app_router
import uvicorn


def include_router(app):
    app.include_router(api_router)
    app.include_router(web_app_router)


def create_tables():
    Base.metadata.create_all(bind=engine)


def start_application():
    tags_metadata = [
        {
            "name": "users",
            "description": "Yeni kullanıcı oluşturabilirsiniz",
        },
        {
            "name": "login",
            "description": "Kullanıcı oluşturduktan sonra login olabilirsiniz",
        },
        {
            "name": "projects",
            "description": "Proje ekleme/çıkarma/güncelleme/silme işlemleri yapabilirsiniz.",
          
        },
        {
            "name": "tasks",
            "description": "İstediğiniz proje için tasklar ekleme/çıkarma/güncelleme/silme işlemleri yapabilirsiniz",
          
        },
        {
            "name": "comments",
            "description": "İstediğiniz task için yorumlar ekleme/çıkarma/güncelleme/silme işlemleri yapabilirsiniz",
        },
    ]
    app = FastAPI(
        title=settings.PROJECT_NAME, version=settings.PROJECT_VERSION,
        openapi_tags=tags_metadata,
        description="""
         ----------
         Hazırlamış olduğum API Restful uygulamasında kullanıcı yeni bir user oluşturur(users/create). Daha sonra login olur.
         Login olduktan sonra projelerini oluşturabilir.Login olmadığı sürece kullanıcı ile ilişkili olan istekler için
         "Not authenticated" hatası alınacaktır.Her proje için ayrı tasklar oluşturulabilir.
         Her bir task için kullanıcı task için yorumlarını da yazabilir. Taskları düzenleyebilir/güncelleyebilir.
         Kullanıcılar birbirlerinin projelerine/tasklarına/yorumlarına erişemez. Kullanıcı sadece kendisine ait olan bilgilere erişebilir. 
         Statü durumları başlama/bitiş tarihleriyle uygun olmadığında tasks/check-task-status çalıştırıldığında celery ile çalışan 
         bu istekte ilgili kişilere mail göndermektedir.(Note:Mail gelmeyebilir.Google May 30, 2022 tarihinde hesap güvenliğini düşür özelliğini kaldırmış.
         Windows için denediğimde mailler geldi. Windows için farklı yollardan ayarlayabilmiştim. Mac için cihazdan yetki gerekmekte.
         )

         ----------
         Task statü için value olarak alındı(1), başladı(2), kontrolde(3), tamamlandı(4) int değerlerini gönderebilirsiniz.
        """
        )
    include_router(app)
    create_tables()
    return app

app = start_application()



@app.on_event("startup")
async def app_startup():
    await check_db_connected()


@app.on_event("shutdown")
async def app_shutdown():
    await check_db_disconnected()

    