from typing import List
from typing import Optional

from api.v1.route_login import get_current_user_from_token
from db.models.user import User
from db.repository.project import create_new_project
from db.repository.project import delete_project_by_id
from db.repository.project import list_projects
from db.repository.project import retreive_project
from db.repository.project import update_project_by_id
from db.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from schemas.project import ProjectCreate, ProjectUpdate, ShowProject
from sqlalchemy.orm import Session


router = APIRouter()


@router.post("/create-project/", response_model=ShowProject)
def create_project(
    project: ProjectCreate,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    task = create_new_project(project=project, db=db, owner_id=current_user.id)
    return task


@router.get("/all", response_model=List[ShowProject])
def read_projects(db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    projects = list_projects(db=db, owner_id=current_user.id)
    return projects


@router.get(
    "/get-project/{id}", response_model=ShowProject
) 
def read_project(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    project = retreive_project(id=id, db=db,owner_id=current_user.id)
    if not project:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Project does not exist",
        )
    return project


@router.put("/update/{id}")
def update_project(id: int, project: ProjectUpdate, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    current_user = 1
    message = update_project_by_id(id=id, project=project, db=db, owner_id=current_user)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Project not found"
        )
    return {"msg": "Successfully updated."}

@router.delete("/delete/{id}")
def delete_project(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token)):
    message = delete_project_by_id(id=id, db=db)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Project not found"
        )
    return {"msg": "Successfully deleted."}
