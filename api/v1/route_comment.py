from typing import List
from api.v1.route_login import get_current_user_from_token
from db.models.user import User
from db.repository.comment import create_new_comment
from db.repository.comment import delete_comment_by_id
from db.repository.comment import list_comments
from db.repository.comment import retreive_comment
from db.repository.comment import update_comment_by_id
from db.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from schemas.comment import CommentCreate, CommentUpdate, ShowComment
from sqlalchemy.orm import Session


router = APIRouter()


@router.post("/task/{id}/create-comment", response_model=ShowComment)
def create_comment(
    id: int,
    comment: CommentCreate,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    cmnt = create_new_comment(id=id, comment=comment, db=db, user_id=current_user.id)
    if not cmnt:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Task does not exist",
        )
    return cmnt


@router.get("/task/{id}/all", response_model=List[ShowComment])
def read_comments(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    comments = list_comments(db=db, id=id,user_id=current_user.id)
    if not comments:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Task does not exist",
        )
    return comments


@router.get(
    "/get-comments/{id}", response_model=ShowComment
) 
def read_comment(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    comment = retreive_comment(id=id, db=db)
    if not comment:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Comment does not exist",
        )
    return comment


@router.put("/update/{id}")
def update_comment(id: int, comment: CommentUpdate, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    message = update_comment_by_id(id=id, comment=comment, db=db)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Comment not found"
        )
    return {"msg": "Successfully updated."}

@router.delete("/delete/{id}")
def delete_comment(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token)):
    message = delete_comment_by_id(id=id, db=db)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Comment not found"
        )
    return {"msg": "Successfully deleted."}
