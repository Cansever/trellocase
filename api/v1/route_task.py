from asyncio import Task
from typing import List
from typing import Optional

from api.v1.route_login import get_current_user_from_token
from db.models.user import User
from db.repository.task import check_status_tasks, create_new_task
from db.repository.task import delete_task_by_id
from db.repository.task import list_tasks
from db.repository.task import retreive_task
from db.repository.task import update_task_by_id
from db.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from schemas.task import TaskCreate, TaskUpdate, ShowTask
from sqlalchemy.orm import Session
from worker import celery_task


router = APIRouter()


@router.post("/project/{id}/create-task", response_model=ShowTask)
def create_task(
    id: int,
    task: TaskCreate,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    task = create_new_task(task=task, db=db, task_owner_id=current_user.id, id=id)
    if not task:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Project does not exist",
        )
    return task


@router.get("/project/{id}/all", response_model=List[ShowTask])
def read_tasks(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    tasks = list_tasks(id=id, db=db, task_owner_id=current_user.id)
    if not tasks:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Project does not exist",
        )
    return tasks


@router.get(
    "/get-task/{id}", response_model=ShowTask
) 
def read_task(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    task = retreive_task(id=id, db=db,owner_id=current_user.id)
    if not task:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"id:{id} - Task does not exist",
        )
    return task
    
@router.get("/check-task-status")
def read_tasks(current_user: User = Depends(get_current_user_from_token),):
    tasks = celery_task.delay("check_task_status", current_user.id)
    return {"result":"Task is completed!"}


@router.put("/update/{id}")
def update_task(id: int, task:  TaskUpdate, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token),):
    
    message = update_task_by_id(id=id, task=task, db=db)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Task not found"
        )
    return {"msg": "Successfully updated."}

@router.delete("/delete/{id}")
def delete_task(id: int, db: Session = Depends(get_db), current_user: User = Depends(get_current_user_from_token)):
    message = delete_task_by_id(id=id, db=db)
    if not message:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"id:{id} - Task not found"
        )
    return {"msg": "Successfully deleted."}
