from api.v1 import route_task
from api.v1 import route_login
from api.v1 import route_user
from api.v1 import route_project
from api.v1 import route_comment


from fastapi import APIRouter


api_router = APIRouter()
api_router.include_router(route_user.router, prefix="/api/users", tags=["users"])
api_router.include_router(route_login.router, prefix="/api/login", tags=["login"])
api_router.include_router(route_project.router, prefix="/api/projects", tags=["projects"])
api_router.include_router(route_task.router, prefix="/api/tasks", tags=["tasks"])
api_router.include_router(route_comment.router, prefix="/api/comments", tags=["comments"])