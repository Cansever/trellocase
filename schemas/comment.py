from datetime import date
from pydantic import BaseModel

 
class CommentCreate(BaseModel):
    description: str


class CommentUpdate(BaseModel):
    description: str


class ShowComment(BaseModel):
    id: int
    description: str
    date_posted: date

    class Config:  # to convert non dict obj to json
        orm_mode = True
