from datetime import date
from pydantic import BaseModel

 
class ProjectCreate(BaseModel):
    title: str


class ProjectUpdate(BaseModel):
    title: str
    is_active: bool

class ShowProject(BaseModel):
    id: int
    title: str
    is_active: bool
    date_posted: date

    class Config:  # to convert non dict obj to json
        orm_mode = True
