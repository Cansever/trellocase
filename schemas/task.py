from asyncio import Task
from datetime import date
from datetime import datetime
from typing import Optional
from enum import Enum
from pydantic import BaseModel

class Status(int, Enum):
    RECEIVED = 1
    STARTED = 2
    IN_CONTROL = 3
    DONE = 4

# shared properties
class TaskBase(BaseModel):
    title: str
    description: str
    status: Optional[int] = 1
  


# this will be used to validate data while creating a Job
class TaskCreate(TaskBase):
    date_planned_start: Optional[date] = None
    date_planned_finish: Optional[date] = None


class TaskUpdate(TaskBase):
    date_planned_start: Optional[date]
    date_planned_finish: Optional[date]
    status: Optional[int] = 1


# this will be used to format the response to not to have id,owner_id etc
class ShowTask(TaskBase):
    id: int
    title: str
    date_posted: date
    status: int
    description: Optional[str] = None
    date_planned_start: Optional[date] = None
    date_planned_finish: Optional[date] = None
    date_completed: Optional[date] = None


    class Config:  # to convert non dict obj to json
        orm_mode = True