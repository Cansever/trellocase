from operator import and_
from db.models.comment import Comment
from db.models.task import Task
from schemas.comment import CommentCreate
from sqlalchemy.orm import Session
from datetime import datetime


def create_new_comment(comment: CommentCreate, db: Session, id: int, user_id:int):
    existing_comment = db.query(Task).filter(and_(Task.id == id, Task.task_owner_id==user_id))
    if not existing_comment.first():
        return False
    comment_object = Comment(**comment.dict(), comment_id=id, date_posted=datetime.now().date())
    db.add(comment_object)
    db.commit()
    db.refresh(comment_object)
    return comment_object


def retreive_comment(id: int, db: Session):
    item = db.query(Comment).filter(Comment.id == id).first()
    return item


def list_comments(db: Session, id: int, user_id: int):
    existing_comment = db.query(Task).filter(and_(Task.id == id, Task.task_owner_id==user_id))
    if not existing_comment.first():
        return False
    tasks = db.query(Comment).filter(Comment.comment_id == id).all()
    return tasks


def update_comment_by_id(id: int, comment: CommentCreate, db: Session):
    existing_comment = db.query(Comment).filter(Comment.id == id)
    if not existing_comment.first():
        return False
    existing_comment.update(comment.__dict__)
    db.commit()
    return True


def delete_comment_by_id(id: int, db: Session):
    existing_comment = db.query(Comment).filter(Comment.id == id)
    if not existing_comment.first():
        return False
    existing_comment.delete(synchronize_session=False)
    db.commit()
    return True
 