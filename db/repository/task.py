from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
from db.models.task import Task
from db.models.project import Project
from schemas.task import TaskCreate
from sqlalchemy.orm import Session
from datetime import datetime
from sqlalchemy import and_
import smtplib
from fastapi import Depends
from db.session import SessionLocal, get_db

def create_new_task(task: TaskCreate, db: Session, id: int, task_owner_id: int ):
    existing_project = db.query(Project).filter(and_(Project.id == id, Project.owner_id==task_owner_id))

    if not existing_project.first():
        return False

    task_object = Task(**task.dict(), task_owner_id=task_owner_id, project_id=id, date_posted=datetime.now().date(), is_active=True)
    db.add(task_object)
    db.commit()
    db.refresh(task_object)
    return task_object

def retreive_task(id: int, db: Session,owner_id: int):
    item = db.query(Task).filter(and_(Task.id == id, Task.task_owner==owner_id)).first()
    return item


def list_tasks(id: int, db: Session, task_owner_id: int):
    tasks = db.query(Task).filter(and_(Task.task_owner_id==task_owner_id, Task.project_id==id)).all()
    return tasks


def check_status_tasks(current_user_id: int):
    new_session = SessionLocal()
    tasks = new_session.query(Task).filter(Task.task_owner_id==current_user_id).all() #get all tasks for user

    #başladı olmayanlar ve tarih varsa geçmesine rağmen statü  değişmeyenler listelere ayrıldı.
    not_started_List = [ element for element in tasks if element.status < 2 and  element.date_planned_start != None and (element.date_planned_start < datetime.now().date()) ] 
    not_completed_List = [ element for element in tasks if element.status < 4 and element.status > 1 and element.date_planned_finish != None and element.date_planned_finish < datetime.now().date() ] 
    #send message for in the lists
    #Google starting May 30, 2022 tarihinde hesap güvenliğini düşür özelliğini kaldırmış. Bu nedenle işletim sistemlerine ayrı app şifreleri oluşturmak gerekiyor.
    #windows için yapmıştım ve mail başarıyla geldi. 
    mail = smtplib.SMTP("smtp.gmail.com", 587)
    mail.ehlo()
    mail.starttls()
    mail.login("adios.destek@gmail.com", "nwiscqmzzncavjxf")
    mesaj = MIMEMultipart()
    for task in not_started_List:
        mesaj["Subject"] = "id:{}-{}-TestCase Statü Uyarı-Task:{}".format(task.id,task.title,task.id)
        mesaj["From"] = "adios.destek@gmail.com"
        mesaj["To"] = task.task_owner.email
        body = "{} id'li task'ın başlangıç tarihi {} başladı(1) statüsünde hala.".format(task.id, task.date_planned_start)
        body_text = MIMEText(body, "plain")  
        mesaj.attach(body_text)
        try:
            mail.sendmail( mesaj["From"], mesaj["To"], mesaj.as_string())
        except Exception as e:
            pass
    for task in not_completed_List:
        mesaj["Subject"] = "id:{}-{}-TestCase Statü Uyarı-Task:{}".format(task.id,task.title,task.id)
        mesaj["From"] = "adios.destek@gmail.com"
        mesaj["To"] = task.task_owner.email
        body = "{} id'li task'ın bitiş tarihi {} completed(4) statüsünde değil.".format(task.id, task.date_planned_finish)
        body_text = MIMEText(body, "plain")  
        mesaj.attach(body_text)
        try:
            mail.sendmail( mesaj["From"], mesaj["To"], mesaj.as_string())
        except Exception as e:
            pass
    
    return True


def update_task_by_id(id: int, task: TaskCreate, db: Session):
    existing_task = db.query(Task).filter(Task.id == id)
    if not existing_task.first():
        return False
    existing_task.update(task.__dict__)
    db.commit()
    return True


def delete_task_by_id(id: int, db: Session):
    existing_task = db.query(Task).filter(Task.id == id)
    if not existing_task.first():
        return False
    existing_task.delete(synchronize_session=False)
    db.commit()
    return True
 