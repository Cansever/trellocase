from operator import and_
from db.models.project import Project
from schemas.project import ProjectCreate
from sqlalchemy.orm import Session
from datetime import datetime


def create_new_project(project: ProjectCreate, db: Session, owner_id: int):
    project_object = Project(**project.dict(), owner_id=owner_id, date_posted=datetime.now().date(), is_active=True)
    db.add(project_object)
    db.commit()
    db.refresh(project_object)
    return project_object


def retreive_project(id: int, db: Session, owner_id: int):
    item = db.query(Project).filter(and_(Project.id == id, Project.owner_id==owner_id)).first()
    return item


def list_projects(db: Session, owner_id: int):
    projects = db.query(Project).filter(Project.owner_id==owner_id).all()
    return projects


def update_project_by_id(id: int, project: ProjectCreate, db: Session, owner_id):
    existing_project = db.query(Project).filter(Project.id == id)
    if not existing_project.first():
        return False
    existing_project.update(project.__dict__)
    db.commit()
    return True


def delete_project_by_id(id: int, db: Session):
    existing_project = db.query(Project).filter(Project.id == id)
    if not existing_project.first():
        return False
    existing_project.delete(synchronize_session=False)
    db.commit()
    return True
 