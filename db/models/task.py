from db.base_class import Base
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship



class Task(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    description = Column(String, nullable=False)
    date_posted = Column(Date)
    date_planned_start = Column(Date)
    date_planned_finish = Column(Date)
    date_completed = Column(Date)
    is_active = Column(Boolean(), default=True)
    status = Column(Integer, nullable=False)

    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship("Project", back_populates="tasks", foreign_keys=[project_id])
    comments = relationship("Comment", back_populates="comment")

    task_owner_id = Column(Integer, ForeignKey('user.id'))
    task_owner = relationship("User", back_populates="tasks", foreign_keys=[task_owner_id])