from db.base_class import Base
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship


class Comment(Base):
    id = Column(Integer, primary_key=True, index=True)
    description = Column(String, nullable=False)
    date_posted = Column(Date)
    comment_id = Column(Integer, ForeignKey("task.id"))
    comment = relationship("Task", back_populates="comments", foreign_keys=[comment_id])
    