from fastapi import APIRouter
from fastapi import Request
from fastapi.responses import RedirectResponse

router = APIRouter(include_in_schema=False)

@router.get("/")
async def home(request: Request):
    return RedirectResponse(url='/docs')

