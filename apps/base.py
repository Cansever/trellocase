from fastapi import APIRouter
from apps import main


api_router = APIRouter()
api_router.include_router(main.router, prefix="", tags=["webapp"])