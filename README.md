# trellocase

---

## Requirements
* Python = 3.8
* PostgreSQL Database = 14
* [Docker](https://www.docker.com/)
* .env file on the project main line

## Description

Bu proje FastApi kullanılarak case ödevi için yapılmıştır.


* This project name: **trellocase**



## Installation
    git clone https://gitlab.com/Cansever/trellocase.git
---

## Run The Project:

### Docker-compose way

#### Example .env file content:

* POSTGRES_DB=trellocase
* POSTGRES_HOST=postgres
* POSTGRES_USER=trello
* POSTGRES_PASSWORD=123456
* POSTGRES_PORT=5432
* SECRET_KEY='=z25@*&h!^w*6&(*gtx!f1x-4bw59s@vc_+qkxzq_s=bd-)m61'

#### Create your new .env file

```shell
$ docker-compose build
$ docker-compose up
```

### show in browser -> http://localhost:8000/docs or http://127.0.0.1:8000/docs
